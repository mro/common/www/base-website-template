
const
  { makeDeferred } = require("@cern/prom");

/**
 * @param  {any} env
 */
async function createApp(env) {
  const Server = (await import("../../src/Server.js")).default;
  var def = makeDeferred();

  env.server = new Server({
    port: 0, basePath: "",
    db: {
      client: "sqlite3", useNullAsDefault: true,
      connection: { filename: ":memory:" }
    }
  });

  env.server.listen(() => def.resolve(undefined));
  return def.promise;
}

module.exports = { createApp };
