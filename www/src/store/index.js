// @ts-check

import { merge } from "lodash";
import Vuex from "vuex";
import Vue from "vue";
import {
  sources as baseSources,
  createStore,
  storeOptions } from "@cern/base-vue";

Vue.use(Vuex);

merge(storeOptions, {
  /* NOTE: declare your store and modules here */
});
export default createStore();

export const sources = merge(baseSources, {
  /* NOTE: declare your global data-sources here */
});
