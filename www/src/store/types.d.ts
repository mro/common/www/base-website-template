
export = AppStore
export as namespace AppStore

declare namespace AppStore {
  interface State {
    user: { username: string, [key: string]: any } | null
  }

  interface UiState {
    showKeyHints: boolean
  }
}
