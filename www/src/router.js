// @ts-check
import Vue from "vue";
import Router from "vue-router";
import { noop, startsWith } from "lodash";

import Home from "./components/Home.vue";
import Admin from "./components/Admin/Admin.vue";

import { sources, default as store } from "./store";
import { BaseLogger as logger } from "@cern/base-vue";

Vue.use(Router);

const router = new Router({
  routes: [
    { name: "Home", path: "/", component: Home },
    { name: "Admin", path: "/admin", component: Admin,
      meta: { navbar: true, isAdmin: true } },

    { path: "/index.html", redirect: "/" },
    { path: "/Layout", redirect: "/" }
  ]
});

router.beforeEach(async function(to, from, next) {
  if (sources.user) {
    await sources.user.prom.catch(noop);
    if (startsWith(to.path, "/admin") && !store.getters.canAdmin) {
      logger.error("editor permission required");
      return next("/");
    }
  }
  return next();
});

export default router;
