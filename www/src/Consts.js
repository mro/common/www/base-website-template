// @ts-check

/* NOTE: declare constants here */

/** @type {string} */
// @ts-ignore
const VER = (typeof VERSION === "undefined") ? "unknown" : VERSION; // jshint ignore:line
export { VER as VERSION };

/** @type {string} */
// @ts-ignore
const TI = (typeof TITLE === "undefined") ? "unknown" : TITLE; // jshint ignore:line
export { TI as TITLE };
