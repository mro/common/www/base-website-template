// @ts-check

import { TITLE, VERSION } from "./Consts";
import Vue from "vue";
import contributors from "../../Contributors.yml";
import libs from "../../Components.yml";

const options = { contributors, libs };

const component = Vue.extend({
  name: "App",
  ...options,
  data() { return { TITLE, VERSION }; }
});

export default component;
