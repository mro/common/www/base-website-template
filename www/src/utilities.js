// @ts-check

/* NOTE: place your utility functions here */

let id = 0;
/**
* @return {string}
*/
export function genId() {
  return "a-" + (++id);
}
