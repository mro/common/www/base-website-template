// @ts-check

import Vue from "vue";

const component = Vue.extend({
  name: "Home"
});
export default component;
