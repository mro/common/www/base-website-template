const
  { karmaConfig } = require('@cern/karma-mocha-webpack');

module.exports = async function(karma) {
  const webpack = (await import("./webpack.config.mjs")).default;
  karma.set(karmaConfig(karma, { browserNoActivityTimeout: 60000, webpack }));
};
