// @ts-check
import express from "express";
import helmet from "helmet";
import serveStatic from "express-static-gzip";
import path from "node:path";
import { makeDeferred } from "@cern/prom";
import logger from "./httpLogger.js";
import { register as authRegister, roleCheck } from "./auth.js";
import { fileURLToPath } from "node:url";

const dirname = path.dirname(fileURLToPath(import.meta.url));

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 */

class Server {
  /**
   * @param {AppServer.Config} config
   */
  constructor(config) {
    this.config = config;
    this.app = express();
    this._prom = this.prepare(config);
  }

  /**
   * @param {AppServer.Config} config
   */
  // eslint-disable-next-line complexity, max-statements
  async prepare(config) {
    if (process.env.NODE_ENV === "production") {
      this.app.use(helmet());
    }
    logger.register(this.app);

    this.router = express.Router();
    const dist = path.join(dirname, "..", "www", "dist");
    this.router.use("/dist", serveStatic(dist, { enableBrotli: true }));

    /* everything after this point is authenticated */
    if (config.auth) {
      await authRegister(this.router, config);
      this.router.use(this.runAuth.bind(this, "user"));
    }

    this.app.set("view engine", "pug");
    this.app.set("views", path.join(dirname, "views"));

    this.router.get("/", (req, res) => res.render("index", config));

    if (config.auth) {
      // protected admin path for admin role only
      this.router.use("/admin", roleCheck.bind(null, [ "admin" ]));
    }

    /* NOTE: declare your additional endpoints here */

    this.app.use(config.basePath, this.router);

    // default route
    this.app.use(function(req, res, next) {
      next({ status: 404, message: "Not Found" });
    });

    // error handler
    this.app.use(function(
      /** @type {any} */ err,
      /** @type {Request} */ req,
      /** @type {Response} */res,
      /** @type {NextFunction} */ next) { /* eslint-disable-line */ /* jshint ignore:line */
      res.locals.message = err.message;
      res.locals.error = req.app.get("env") === "development" ? err : {};
      res.locals.status = err.status || 500;
      res.status(err.status || 500);
      res.render("error", config);
    });
  }

  close() {
    if (this.server) {
      this.server.close();
      this.server = null;
    }
  }

  /**
   * @param {string|string[]} role
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  runAuth(role, req, res, next) {
    if ([ "/dist", "/stub" ].some((p) => req.path.startsWith(p))) {
      return next();
    }
    return roleCheck(role, req, res, next);
  }

  /**
   * @param {() => any} cb
   */
  async listen(cb) {
    await this._prom;
    const def = makeDeferred();
    /* we're called as a main, let's listen */
    var server = this.app.listen(this.config.port, () => {
      this.server = server;
      def.resolve(undefined);
      return cb?.();
    });
    return def.promise;
  }

  address() {
    if (this.server) {
      return this.server.address();
    }
    return null;
  }
}

export default Server;
