// @ts-check
import morgan from "morgan";
import d from "debug";

const debug = d("app:http");

/**
 * @typedef {import('express').IRouter} IRouter
 */

const stream = {
  write: (/** @type {string} */ message) => debug(message?.trim())
};

export default {
  /**
   * @param {IRouter} app
   */
  register(app) {
    if (debug.enabled) {
      debug("http logger enabled");
      app.use(morgan(
        /* eslint-disable-next-line max-len */
        ":method :url :status :response-time ms - :res[content-length] bytes",
        { stream, skip: (req) => req.path?.endsWith(".websocket") }));

      app.use(morgan("WS :url",
        { stream, immediate: true,
          skip: (req) => !req.path?.endsWith(".websocket") }));
    }
  }
};
