// @ts-check
import Server from "./Server.js";
import config from "./config.js";
import esMain from "es-main";

const server = new Server(config);

/* pm2 esm support */
process.argv[1] = process.env?.pm_exec_path ?? process.argv[1];

if (esMain(import.meta)) {
  /* we're called as a main, let's listen */
  await server.listen(() => {
    console.log(`Server listening on port http://localhost:${config.port}`);
  });
}
/* export our server */
export default server;
