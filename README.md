# Web Application template

This is a template for Web-Application development

This application is developped according to our [guidelines](https://mro-dev.web.cern.ch/docs/std/en-smm-apc-web-guidelines.html).

# Documentation

# Build

To build this application application (assuming that Node.js is installed on your machine):
```bash
# Run webpack and bundle things in /dist
npm run build

# Serve pages on port 8080
npm run serve
```

# Deploy

To deploy the example application, assuming that `oc` is configured and connected
on the proper project:
```bash
# Modify parameters in 'config.env' file properly
# Do never commit passwords/secrets!
oc process --local --parameters -f deploy/openshift-dc.template.yaml # for details about parameters
vim config.env

# Install 'js-yaml' with pass extension
npm install --save-dev git+https://gitlab.cern.ch/mro/common/www/js-yaml.git

# Generate the configuration and apply it:
# 1. process the deployment template file ('openshift-dc.template.yaml') into a list of openshift resources;
# 2. inject the application config (including any credentials);
# 3. apply the resulting configuration to a pod
oc process --local --param-file=deploy/config.env -o 'yaml' -f deploy/openshift-dc.template.yaml > deploy/openshift-dc.yaml
oc-gen deploy/openshift-dc.yaml deploy/config*.yml | oc apply -f -
```
